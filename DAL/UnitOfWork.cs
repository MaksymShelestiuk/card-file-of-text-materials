﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// Provides access to repositories through individual properties. 
    /// Defines a common context for both repositories.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CardFileContext _context;
        private ArticleRepository _articleRepository;
        private CategoryRepository _categoryRepository;

        /// <summary>
        /// Initializes private readonly field with the DbContext
        /// </summary>
        /// <param name="context">DbContext</param>
        public UnitOfWork(CardFileContext context)
        {
            _context = context;
        }
        public IArticleRepository Articles
        {
            get
            {
                if (_articleRepository == null)
                    _articleRepository = new ArticleRepository(_context);
                return _articleRepository;
            }
        }

        public IRepository<Category> Categories
        {
            get
            {
                if (_categoryRepository == null)
                    _categoryRepository = new CategoryRepository(_context);
                return _categoryRepository;
            }
        }

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
