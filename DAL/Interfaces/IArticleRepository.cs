﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IArticleRepository:IRepository<Article>
    {
        Task<IEnumerable<Article>> GetAllWithDetailsAsync();
        Task<Article> GetWithDetailsByIdAsync(int id);
        Task<IEnumerable<Article>> GetAllWithDetailsByUserIdAsync(int userProfileId);
        Task<IEnumerable<Article>> GetAllWithDetailsByCategoryIdAsync(int categoryId);
    }
}
