﻿using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly CardFileContext _db;
        public CategoryRepository(CardFileContext context)
        {
            _db = context;
        }
        public async Task<Category> CreateAsync(Category item)
        {
            var result = _db.Categories.Add(item);
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _db.Categories.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _db.Categories.Remove(item);
            }
            return result;
        }

        public IEnumerable<Category> Find(Func<Category, Boolean> predicate)
        {
            return _db.Categories.Where(predicate).ToList();
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            return await _db.Categories.ToListAsync();
        }

        public async Task<Category> GetByIdAsync(int id)
        {
            return await _db.Categories.FindAsync(id);
        }

        public async Task<bool> UpdateAsync(Category item)
        {
            var entity = _db.Categories.Attach(item);
            _db.Entry(item).State = EntityState.Modified;
            return entity != null;
        }
    }
}
