﻿using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly CardFileContext _db;
        public ArticleRepository(CardFileContext context)
        {
            _db = context;
        }
        public async Task<Article> CreateAsync(Article item)
        {
            var result =_db.Articles.Add(item);
            return result.Entity;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _db.Articles.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _db.Articles.Remove(item);
            }
            return result;
        }

        public IEnumerable<Article> Find(Func<Article, Boolean> predicate)
        {
            return _db.Articles.Include(u => u.UserProfile)
                .Include(c => c.Category).Where(predicate).ToList();
        }

        public async Task<IEnumerable<Article>> GetAllAsync()
        {
            return await _db.Articles.ToListAsync();
        }

        public async Task<Article> GetByIdAsync(int id)
        {
            return await _db.Articles.FindAsync(id);
        }

        public async Task<bool> UpdateAsync(Article item)
        {
            var entity = _db.Articles.Attach(item);
            _db.Entry(item).State = EntityState.Modified;
            return entity != null;
        }

        public async Task<IEnumerable<Article>> GetAllWithDetailsAsync()
        {
            return await _db.Articles.Include(u => u.UserProfile)
                .Include(c => c.Category).ToListAsync();
        }

        public async Task<Article> GetWithDetailsByIdAsync(int id)
        {
            return await _db.Articles.Include(u => u.UserProfile)
                .Include(c => c.Category)
                .SingleOrDefaultAsync(a => a.ArticleId == id);
        }

        public async Task<IEnumerable<Article>> GetAllWithDetailsByUserIdAsync(int userProfileId)
        {
            return await _db.Articles.Include(u => u.UserProfile)
                .Include(c => c.Category)
                .Where(a => a.UserProfileId == userProfileId).ToListAsync();
        }
        public async Task<IEnumerable<Article>> GetAllWithDetailsByCategoryIdAsync(int categoryId)
        {
            return await _db.Articles.Include(u => u.UserProfile)
                .Include(c => c.Category)
                .Where(a => a.CategoryId == categoryId).ToListAsync();
        }
    }
}
