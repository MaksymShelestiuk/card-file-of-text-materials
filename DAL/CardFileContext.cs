﻿using System;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    /// <summary>
    /// The context used by the Entity Framework with identity
    /// </summary>
    public class CardFileContext: IdentityDbContext<UserProfile, Role, int>
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Category> Categories { get; set; }
        /// <summary>
        /// Initializes a new instance of the db context
        /// </summary>
        /// <param name="options">The options to be used by the DbContext</param>
        public CardFileContext(DbContextOptions<CardFileContext> options) : base(options)
        {}
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Article>().ToTable("Article");
            builder.Entity<Category>().ToTable("Category");
            builder.Entity<UserProfile>().ToTable("UserProfile");
        }
    }
}
