﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class UserProfile : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
    }
}
