﻿using System;
using System.Net;

namespace TextCardFileWebApi.Resources
{
    /// <summary>
    /// The exception class to represent an HTTP exception
    /// </summary>
    public class HttpException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }
        public HttpException(HttpStatusCode statusCode, string message)
            : base(message)
        {
            StatusCode = statusCode;
        }
    }
}