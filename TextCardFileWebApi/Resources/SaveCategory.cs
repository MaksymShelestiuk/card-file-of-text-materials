﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextCardFileWebApi.Resources
{
    public class SaveCategory
    {
        public string Name { get; set; }
    }
}
