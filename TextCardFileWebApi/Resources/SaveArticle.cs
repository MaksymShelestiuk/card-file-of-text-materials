﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextCardFileWebApi.Resources
{
    public class SaveArticle
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public DateTime DateOfPublication { get; set; }
        public int CategoryId { get; set; }
    }
}
