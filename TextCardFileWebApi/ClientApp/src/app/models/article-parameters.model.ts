export class ArticleParameters {
  constructor(pageNumber?: number, pageSize?: number, orderBy?: string,
    minDate?: string, maxDate?: string, searchTitle?: string) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.orderBy = orderBy;
    this.minDate = minDate;
    this.maxDate = maxDate;
    this.searchTitle = searchTitle;
  }
  public pageNumber: number;
  public pageSize: number;
  public orderBy: string;
  public minDate: string;
  public maxDate: string;
  public searchTitle: string;
}
