"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArticleParameters = void 0;
var ArticleParameters = /** @class */ (function () {
    function ArticleParameters(pageNumber, pageSize, orderBy, minDate, maxDate, searchTitle) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.orderBy = orderBy;
        this.minDate = minDate;
        this.maxDate = maxDate;
        this.searchTitle = searchTitle;
    }
    return ArticleParameters;
}());
exports.ArticleParameters = ArticleParameters;
//# sourceMappingURL=page.model.js.map