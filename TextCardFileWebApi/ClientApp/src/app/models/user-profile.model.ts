export class UserProfile {
  constructor(id?: number, firstName?: string, secondName?: string,
    userName?: string, emailName?: string, phoneNumber?: string) {
    this.id = id;
    this.firstName = firstName;
    this.secondName = secondName;
    this.userName = userName;
    this.emailName = emailName;
    this.phoneNumber = phoneNumber;
  }
  public id: number;
  public firstName: string;
  public secondName: string;
  public userName: string;
  public emailName: string;
  public phoneNumber: string;
}
