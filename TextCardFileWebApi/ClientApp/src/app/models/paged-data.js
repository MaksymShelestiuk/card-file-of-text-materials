"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PagedData = void 0;
var PagedData = /** @class */ (function () {
    function PagedData(currentPage, totalPages, pageSize, totalCount, data) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.data = data;
    }
    return PagedData;
}());
exports.PagedData = PagedData;
//# sourceMappingURL=paged-data.js.map