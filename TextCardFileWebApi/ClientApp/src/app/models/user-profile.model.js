"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserProfile = void 0;
var UserProfile = /** @class */ (function () {
    function UserProfile(id, firstName, secondName, userName, emailName, phoneNumber) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.userName = userName;
        this.emailName = emailName;
        this.phoneNumber = phoneNumber;
    }
    return UserProfile;
}());
exports.UserProfile = UserProfile;
//# sourceMappingURL=user-profile.model.js.map