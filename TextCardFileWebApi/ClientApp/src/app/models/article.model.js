"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Article = void 0;
var Article = /** @class */ (function () {
    function Article(articleId, title, description, content, dateOfPublication, category, userProfile) {
        this.articleId = articleId;
        this.title = title;
        this.description = description;
        this.content = content;
        this.dateOfPublication = dateOfPublication;
        this.category = category;
        this.userProfile = userProfile;
    }
    return Article;
}());
exports.Article = Article;
//# sourceMappingURL=article.model.js.map