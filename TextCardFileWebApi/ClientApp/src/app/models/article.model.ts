import { Category } from '../models/category.model'
import { UserProfile } from '../models/user-profile.model';

export class Article {
  constructor(articleId?: number, title?: string, description?: string,
    content?: string, dateOfPublication?: string, category?: Category,
    userProfile?: UserProfile) {
    this.articleId = articleId;
    this.title = title;
    this.description = description;
    this.content = content;
    this.dateOfPublication = dateOfPublication;
    this.category = category;
    this.userProfile = userProfile;
  }
  public articleId: number;
  public title: string;
  public description: string;
  public content: string;
  public dateOfPublication: string;
  public category: Category;
  public userProfile: UserProfile;
}
