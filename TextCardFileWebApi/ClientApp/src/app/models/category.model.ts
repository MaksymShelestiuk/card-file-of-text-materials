export class Category {
  constructor(categoryId?: number, name?: string) {
    this.categoryId = categoryId;
    this.name = name;
  }
  public categoryId: number;
  public name: string;
}
