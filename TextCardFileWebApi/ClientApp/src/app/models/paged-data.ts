export class PagedData<T> {
  constructor(currentPage?: number, totalPages?: number, pageSize?: number,
    totalCount?: number, data?: Array<T>) {
    this.currentPage = currentPage;
    this.totalPages = totalPages;
    this.pageSize = pageSize;
    this.totalCount = totalCount;
    this.data = data;
  }
  public currentPage: number;
  public totalPages: number;
  public pageSize: number;
  public totalCount: number;
  public data: Array<T>;
}
