import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { Observable, Subject, forkJoin, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { Article } from '../models/article.model';
import { ArticleParameters } from '../models/article-parameters.model';
import { PagedData } from '../models/paged-data';

@Injectable()
export class ArticleService {
  constructor(private http: HttpClient) { }

  getAllArticles(params: ArticleParameters) {
    let pagination = 'PageNumber=${params.pageNumber}&PageSize=${params.pageSize}';
    let filtration = 'MinDate=${params.minDate}&MaxDate=${params.maxDate}';
    let search = 'SearchTitle=${params.searchTitle}';
    let sort = 'OrderBy=${params.orderBy}'
    return this.http.get<any>
      (environment.baseUrl + '/api/Articles?' + pagination +
        '&' + filtration + '&' + search + '&' + sort).pipe(map((res) => res));
  }
  getArticleById(id: number) {
    return this.http.get<any>(environment.baseUrl + '/api/Articles' + id)
      .pipe(map((res) => res), catchError((e) => of({ success: false })));
  }
  getArticlesForUser(userId: number) {
    return this.http.get<any>(environment.baseUrl + '/api/Articles/ForUserProfile/' + userId)
      .pipe(map((res) => res), catchError((e) => of({ success: false })));
  }
  getArticlesForCategory(categoryId: number) {
    return this.http.get<any>(environment.baseUrl + '/api/Articles/ForCategory/' + categoryId)
      .pipe(map((res) => res), catchError((e) => of({ success: false })));
  }
  createArticle(article: Article) {
    return this.http.post<any>(environment.baseUrl + '/api/Articles/', article)
      .pipe(map((res) => res), catchError((e) => of({ success: false })));
  }
  updateArticle(id: number, body: any) {
    return this.http.put<any>(environment.baseUrl + '/api/Articles/', body)
      .pipe(map((res) => res), catchError((e) => of({ success: false })));
  }
  deleteArticle(id: number) {
    return this.http.delete<any>(environment.baseUrl + '/api/Articles/' + id)
      .pipe(map((res) => res), catchError((e) => of({ success: false })));
  }
}
