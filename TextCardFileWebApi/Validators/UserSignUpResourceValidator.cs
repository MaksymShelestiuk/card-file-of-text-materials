﻿using FluentValidation;
using System.Text.RegularExpressions;
using TextCardFileWebApi.Administration.Resources;
using TextCardFileWebApi.Resources;
namespace TextCardFileWebApi.Validators
{
    public class UserSignUpResourceValidator : AbstractValidator<UserSignUpResource>
    {
        public UserSignUpResourceValidator()
        {
            RuleFor(u => u.Email)
                .NotEmpty()
                .EmailAddress();
            RuleFor(u => u.FirstName)
                .NotEmpty()
                .Must(fname => fname.Length > 0 && Regex.IsMatch(fname, @"^[a-zA-Z]+$") && char.IsUpper(fname[0]))
                .WithMessage("FirstName should contain only letters with Upper First letter");
            RuleFor(u => u.SecondName)
                .NotEmpty()
                .Must(sname => sname.Length > 0 && Regex.IsMatch(sname, @"^[a-zA-Z]+$") && char.IsUpper(sname[0]))
                .WithMessage("SecondName should contain only letters with Upper First letter");
        }
    }
}
