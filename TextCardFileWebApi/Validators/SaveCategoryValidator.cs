﻿using FluentValidation;
using TextCardFileWebApi.Resources;

namespace TextCardFileWebApi.Validators
{
    public class SaveCategoryValidator : AbstractValidator<SaveCategory>
    {
        public SaveCategoryValidator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .MinimumLength(2)
                .MaximumLength(25)
                .Must(name => char.IsLetter(name[0]) && char.IsUpper(name[0]));
        }
    }
}
