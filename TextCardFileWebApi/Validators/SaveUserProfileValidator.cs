﻿using FluentValidation;
using System.Text.RegularExpressions;
using TextCardFileWebApi.Resources;

namespace TextCardFileWebApi.Validators
{
    public class SaveUserProfileValidator : AbstractValidator<SaveUserProfile>
    {
        public SaveUserProfileValidator()
        {
            RuleFor(u => u.Email)
                .NotEmpty()
                .EmailAddress();
            RuleFor(u => u.FirstName)
                .NotEmpty()
                .Must(fname => fname.Length > 0 && Regex.IsMatch(fname, @"^[a-zA-Z]+$") && char.IsUpper(fname[0]));
            RuleFor(u => u.SecondName)
                .NotEmpty()
                .Must(sname => sname.Length > 0 && Regex.IsMatch(sname, @"^[a-zA-Z]+$") && char.IsUpper(sname[0]));
            RuleFor(u => u.PhoneNumber)
                .Length(4, 14)
                .Must(phone => Regex.IsMatch(phone, @"^[0-9+]+$"));
        }
    }
}