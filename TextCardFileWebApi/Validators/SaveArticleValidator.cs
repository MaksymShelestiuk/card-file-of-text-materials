﻿using FluentValidation;
using System.Text.RegularExpressions;
using TextCardFileWebApi.Resources;

namespace TextCardFileWebApi.Validators
{
    public class SaveArticleValidator : AbstractValidator<SaveArticle>
    {
        public SaveArticleValidator()
        {
            RuleFor(a => a.Title)
                .NotEmpty()
                .Length(3, 80)
                .Must(title => char.IsLetter(title[0]) && char.IsUpper(title[0]));
            RuleFor(a => a.Description)
                .NotEmpty()
                .MinimumLength(5);
            RuleFor(a => a.Content)
                .NotEmpty()
                .MinimumLength(50);
            RuleFor(a => a.CategoryId)
                .NotNull()
                .GreaterThan(0);
        }
    }
}