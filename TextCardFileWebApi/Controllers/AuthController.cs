﻿using AutoMapper;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TextCardFileWebApi.Administration;
using TextCardFileWebApi.Administration.Resources;
using TextCardFileWebApi.Administration.Settings;
using TextCardFileWebApi.Resources;
using TextCardFileWebApi.Validators;

namespace TextCardFileWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<UserProfile> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly JwtSettings _jwtSettings;
        private readonly IMapper _mapper;
        /// <summary>
        /// Initializes controller
        /// </summary>
        /// <param name="mapper">Provides functionality for mapping</param>
        /// <param name="userManager">Provides functionality for managing users</param>
        /// <param name="roleManager">Provides functionality for managing roles</param>
        /// <param name="jwtSettings">Used to access JwtSettings for the lifetime of a request</param>
        public AuthController(IMapper mapper, UserManager<UserProfile> userManager, RoleManager<Role> roleManager,
            IOptionsSnapshot<JwtSettings> jwtSettings)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _jwtSettings = jwtSettings.Value;
        }

        [HttpPost("SignUp")]
        public async Task<IActionResult> SignUp(UserSignUpResource userSignUpResource)
        {
            var validator = new UserSignUpResourceValidator();
            var validationResult = await validator.ValidateAsync(userSignUpResource);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);
            var user = _mapper.Map<UserSignUpResource, UserProfile>(userSignUpResource);
            var userCreateResult = await _userManager.CreateAsync(user, userSignUpResource.Password);

            if (userCreateResult.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "User");
                return Created(string.Empty, string.Empty);
            }
            return Problem(userCreateResult.Errors.First().Description, null, 500);
        }

        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(UserLoginResource userLoginResource)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Email == userLoginResource.Email);
            if (user is null)
            {
                throw new HttpException(HttpStatusCode.NotFound, "User was not found.");
            }

            var userSignInResult = await _userManager.CheckPasswordAsync(user, userLoginResource.Password);
            if (userSignInResult)
            {
                var roles = await _userManager.GetRolesAsync(user);
                return Ok(JwtHelper.GenerateJwt(user, roles, _jwtSettings));
            }
            throw new HttpException(HttpStatusCode.BadRequest, "Email or password incorrect.");
        }

        [HttpPost("Roles")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateRole(string roleName)
        {
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Role name should be provided.");
            }
            var newRole = new Role
            {
                Name = roleName
            };
            var roleResult = await _roleManager.CreateAsync(newRole);
            if (roleResult.Succeeded)
            {
                return Ok();
            }
            return Problem(roleResult.Errors.First().Description, null, 500);
        }

        [HttpPost("User/{userEmail}/Role")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddUserToRole(string userEmail, [FromBody] string roleName)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == userEmail);
            var result = await _userManager.AddToRoleAsync(user, roleName);
            if (result.Succeeded)
            {
                return Ok();
            }
            return Problem(result.Errors.First().Description, null, 500);
        }
    }
}
