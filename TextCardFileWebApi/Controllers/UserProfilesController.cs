﻿using AutoMapper;
using BLL.Dto;
using BLL.Extensions;
using BLL.Interfaces;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TextCardFileWebApi.Resources;
using TextCardFileWebApi.Validators;

namespace TextCardFileWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfilesController : ControllerBase
    {
        private readonly IUserProfileService _userProfileService;
        private readonly UserManager<UserProfile> _userManager;
        private readonly IMapper _mapper;
        public UserProfilesController (IMapper mapper, IUserProfileService userProfileService,
            UserManager<UserProfile> userManager)
        {
            _userProfileService = userProfileService;
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpGet("")]
        public ActionResult<IEnumerable<UserProfile>> GetAllUserProfiles([FromQuery] UserProfileParameters parameters)
        {
            var users = _userProfileService.GetAll(parameters);
            Response.Headers.Add("Pagination-info", JsonConvert.SerializeObject(users.GetMetaData()));
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserProfile>> GetUserProfileById(int id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            return Ok(user);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<UserProfile>> UpdateUserProfile(int id, [FromBody] SaveUserProfile saveUserProfile)
        {
            var validator = new SaveUserProfileValidator();
            var validationResult = await validator.ValidateAsync(saveUserProfile);
            if (id == 0 || !validationResult.IsValid)
                return BadRequest(validationResult.Errors);
            var saveUserProfileDto = _mapper.Map<SaveUserProfile, SaveUserProfileDto>(saveUserProfile);
            var userUpdateResult = await _userProfileService.UpdateAsync(id, saveUserProfileDto);
            if (userUpdateResult.Succeeded)
            {
                return Ok(_userManager.FindByIdAsync(id.ToString()));
            }
            return Problem(userUpdateResult.Errors.First().Description, null, 500);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteUserProfile(int id)
        {
            var userToDelete = await _userManager.FindByIdAsync(id.ToString());
            var success = await _userManager.DeleteAsync(userToDelete);
            if (!success.Succeeded)
                throw new HttpException(HttpStatusCode.BadRequest, "UserProfile wasn't deleted. Check if Id is correct");
            else
            {
                return NoContent();
            }
        }
    }
}
