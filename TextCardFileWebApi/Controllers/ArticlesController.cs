﻿using AutoMapper;
using BLL.Dto;
using BLL.Extensions;
using BLL.Interfaces;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TextCardFileWebApi.Resources;
using TextCardFileWebApi.Validators;

namespace TextCardFileWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly UserManager<UserProfile> _userManager;
        private readonly IArticleServiceAsync _articleService;
        private readonly IMapper _mapper;
        public ArticlesController(IArticleServiceAsync articleService, IMapper mapper, 
            UserManager<UserProfile> userManager)
        {
            _articleService = articleService;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet("")]
        public async Task<ActionResult<PagedList<ArticleDto>>> GetAllArticles([FromQuery] ArticleParameters articleParameters)
        {
            if (!articleParameters.ValidDate)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Max Date of publication cannot be less than min Date.");
            }
            PagedList<ArticleDto> articles = await _articleService.GetAllAsync(articleParameters);
            Response.Headers.Add("Pagination-info", JsonConvert.SerializeObject(articles.GetMetaData()));
            return Ok(articles);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ArticleDto>> GetArticleById(int id)
        {
            var article = await _articleService.GetByIdAsync(id);
            return Ok(article);
        }

        [HttpGet("ForUserProfile/{userId}")]
        public async Task<ActionResult<IEnumerable<ArticleDto>>> GetArticlesForUser(int userId, [FromQuery] ArticleParameters articleParameters)
        {
            if (!articleParameters.ValidDate)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Max Date of publication cannot be less than min Date.");
            }
            var articles = await _articleService.GetAllArticlesByUserIdAsync(userId, articleParameters);
            Response.Headers.Add("Pagination-info", JsonConvert.SerializeObject(articles.GetMetaData()));
            return Ok(articles);
        }

        [HttpGet("ForCategory/{categoryId}")]
        public async Task<ActionResult<IEnumerable<ArticleDto>>> GetArticlesForCategory(int categoryId, [FromQuery] ArticleParameters articleParameters)
        {
            if (!articleParameters.ValidDate)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Max Date of publication cannot be less than min Date.");
            }
            var articles = await _articleService.GetAllArticlesByCategoryIdAsync(categoryId, articleParameters);
            Response.Headers.Add("Pagination-info", JsonConvert.SerializeObject(articles.GetMetaData()));
            return Ok(articles);
        }

        [HttpPost("")]
        [Authorize(Roles = "Admin, User")]
        public async Task<ActionResult<ArticleDto>> CreateArticle([FromBody] SaveArticle saveArticle)
        {
            var validator = new SaveArticleValidator();
            var validationResult = await validator.ValidateAsync(saveArticle);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            var articleToCreate = _mapper.Map<SaveArticle, ArticleDto>(saveArticle);
            articleToCreate.DateOfPublication = DateTime.Now;
            articleToCreate.UserProfile = await _userManager.GetUserAsync(User);
            var newArticle = await _articleService.CreateAsync(articleToCreate, saveArticle.CategoryId);
            var result = await _articleService.GetByIdAsync(newArticle.ArticleId);
            return CreatedAtAction(nameof(GetArticleById),result);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<ArticleDto>> UpdateArticle(int id, [FromBody] SaveArticle saveArticle)
        {
            var validator = new SaveArticleValidator();
            var validationResult = await validator.ValidateAsync(saveArticle);
            if (id == 0 || !validationResult.IsValid)
                return BadRequest(validationResult.Errors);
            var article = _mapper.Map<SaveArticle, ArticleDto>(saveArticle);
            article.ArticleId = id;
            article.DateOfPublication = DateTime.Now;
            await _articleService.UpdateAsync(article, saveArticle.CategoryId);
            var result = await _articleService.GetByIdAsync(article.ArticleId);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteArticle(int id)
        {
            var success = await _articleService.DeleteAsync(id);
            if (!success)
                throw new HttpException(HttpStatusCode.BadRequest, "Article wasn't deleted. Check if articleId is correct");
            else
            {
                return NoContent();
            }
        }
    }
}
