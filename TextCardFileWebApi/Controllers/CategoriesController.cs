﻿using AutoMapper;
using BLL.Dto;
using BLL.Interfaces;
using BLL.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using TextCardFileWebApi.Resources;
using TextCardFileWebApi.Validators;
using System.Net;

namespace TextCardFileWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryServiceAsync _categoryService;
        private readonly IMapper _mapper;
        public CategoriesController(ICategoryServiceAsync categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }
        
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<CategoryDto>>> GetAllCategories([FromQuery] CategoryParameters categoryParameters)
        {
            var categories = await _categoryService.GetAllAsync(categoryParameters);
            return Ok(categories);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDto>> GetCategoryById(int id)
        {
            var category = await _categoryService.GetByIdAsync(id);
            return Ok(category);
        }
        
        [HttpPost("")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<CategoryDto>> CreateCategory([FromBody] SaveCategory saveCategory)
        {
            var validator = new SaveCategoryValidator();
            var validationResult = await validator.ValidateAsync(saveCategory);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            var categoryToCreate = _mapper.Map<SaveCategory, CategoryDto>(saveCategory);

            var newCategory = await _categoryService.CreateAsync(categoryToCreate);
            var result = await _categoryService.GetByIdAsync(newCategory.CategoryId);
            return CreatedAtAction(nameof(GetCategoryById), result);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<CategoryDto>> UpdateCategory(int id, [FromBody] SaveCategory saveCategory)
        {
            var validator = new SaveCategoryValidator();
            var validationResult = await validator.ValidateAsync(saveCategory);
            if (id == 0 || !validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            var category = _mapper.Map<SaveCategory, CategoryDto>(saveCategory);
            category.CategoryId = id;

            await _categoryService.UpdateAsync(category);
            var result = await _categoryService.GetByIdAsync(category.CategoryId);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var success = await _categoryService.DeleteAsync(id);
            if (!success)
                throw new HttpException(HttpStatusCode.BadRequest, "Category wasn't deleted. Check if categoryId is correct");
            else
            {
                return NoContent();
            }
        }
    }
}
