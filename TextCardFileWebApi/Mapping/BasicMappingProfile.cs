﻿using AutoMapper;
using BLL.Dto;
using DAL.Entities;
using TextCardFileWebApi.Administration.Resources;
using TextCardFileWebApi.Resources;

namespace TextCardFileWebApi.Mapping
{
    public class BasicMappingProfile : Profile
    {
        public BasicMappingProfile()
        {
            CreateMap<SaveArticle, ArticleDto>();
            CreateMap<SaveCategory, CategoryDto>();
            CreateMap<UserSignUpResource, UserProfile>()
                .ForMember(u => u.UserName, opt => opt.MapFrom(ur => ur.Email.Remove(ur.Email.IndexOf("@"))));
            CreateMap<SaveUserProfile, SaveUserProfileDto>();
        }
    }
}
