﻿using TextCardFileWebApi.Resources;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace TextCardFileWebApi.Extensions
{
    /// <summary>
    /// Provides Extension method to create .NET Core built-in native middleware 
    /// to handle an exception that might occur in a request.
    /// </summary>
    public static class ExceptionMiddlewareExtensions
    {
        /// <summary>
        /// Adds a middleware to the pipeline that will catch exceptions, log them.
        /// </summary>
        /// <param name="app">IApplicationBuilder to configure request pipeline</param>
        /// <returns></returns>
        public static IApplicationBuilder AddGlobalExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                    var exception = errorFeature.Error;

                    var errorResponse = new ErrorResponse();

                    if (exception is HttpException httpException)
                    {
                        errorResponse.StatusCode = httpException.StatusCode;
                        errorResponse.Message = httpException.Message;
                    }

                    context.Response.StatusCode = (int)errorResponse.StatusCode;
                    context.Response.ContentType = "application/json";
                    // Writing the Error Response to the response body
                    await context.Response.WriteAsync(errorResponse.ToJsonString());
                });
            });
            return app;
        }
    }
}
