﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dto
{
    public class CategoryDto
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }
}
