﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dto
{
    public class ArticleDto
    {
        public int ArticleId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public DateTime DateOfPublication { get; set; }
        public UserProfile UserProfile { get; set; }
        public Category Category { get; set; }
    }
}
