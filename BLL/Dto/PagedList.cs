﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Dto
{
	/// <summary>
	/// Provides functionality to give response with paginated data
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class PagedList<T>
	{
		public int CurrentPage { get; private set; }
		public int TotalPages { get; private set; }
		public int PageSize { get; private set; }
		public int TotalCount { get; private set; }
		public IEnumerable<T> Data { get; set; }

		public bool HasPrevious => CurrentPage > 1;
		public bool HasNext => CurrentPage < TotalPages;

		/// <summary>
		/// Initializes paginated collection and pagination info.
		/// Also calculates total number of pages rounded to greater integer value
		/// </summary>
		/// <param name="items">Collection of items to be paginated</param>
		/// <param name="count">Length of collection of items</param>
		/// <param name="pageNumber">Current number of page to be represented</param>
		/// <param name="pageSize">Number of items per page</param>
		public PagedList(IEnumerable<T> items, int count, int pageNumber, int pageSize)
		{
			Data = items;
			TotalCount = count;
			PageSize = pageSize;
			CurrentPage = pageNumber;
			TotalPages = (int)Math.Ceiling(count / (double)pageSize);
		}

		/// <summary>
		/// Skips first ((N - 1) * S) results and then take the next S and 
		/// return them to the caller (N - pageNumber, S - pageSize).
		/// </summary>
		/// <param name="source">Collection of items to be paginated</param>
		/// <param name="pageNumber">Current number of page to be represented</param>
		/// <param name="pageSize">Number of items per page</param>
		/// <returns>Collection of items with the length of pageSize and for the right pageNumber</returns>
		public static PagedList<T> ToPagedList(IEnumerable<T> source, int pageNumber, int pageSize)
		{
			var count = source.Count();
			source = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
			var res = new PagedList<T>(source, count, pageNumber, pageSize);
			return res;
		}
	}
}
