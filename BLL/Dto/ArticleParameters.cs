﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dto
{
    public class ArticleParameters : QueryParameters
    {
        // for DateOfPublication filtration
        public DateTime MinDate { get; set; } = DateTime.Now.AddYears(-20);
        public DateTime MaxDate { get; set; } = DateTime.Now;
        public bool ValidDate => MaxDate > MinDate;

        // for search
        public string SearchTitle { get; set; }

        // for sorting
        public ArticleParameters()
        {
            OrderBy = "DateOfPublication";
        }
    }
}
