﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dto
{
    public class UserProfileParameters : QueryParameters
    {
        // for search
        public string SearchUserName { get; set; }
    }
}
