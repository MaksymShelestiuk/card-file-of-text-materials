﻿namespace BLL.Dto
{
    public class SaveUserProfileDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
