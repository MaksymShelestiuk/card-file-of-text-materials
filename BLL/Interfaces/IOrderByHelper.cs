﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface IOrderByHelper<T>
    {
        IEnumerable<T> ApplySort(IEnumerable<T> items, string orderByQuery);
    }
}
