﻿using BLL.Dto;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserProfileService
    {
        PagedList<UserProfile> GetAll(UserProfileParameters parameters);
        Task<IdentityResult> UpdateAsync(int userId, SaveUserProfileDto saveUserProfile);
        Task<UserProfile> GetByIdAsync(int id);
    }
}
