﻿using BLL.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICategoryServiceAsync : IValidation
    {
        Task<IEnumerable<CategoryDto>> GetAllAsync(CategoryParameters categoryParameters);
        Task<CategoryDto> CreateAsync(CategoryDto item);
        Task<bool> DeleteAsync(int id);
        Task<CategoryDto> GetByIdAsync(int id);
        abstract Task<bool> UpdateAsync(CategoryDto item);
        IEnumerable<CategoryDto> Find(Func<CategoryDto, Boolean> predicate);
    }
}
