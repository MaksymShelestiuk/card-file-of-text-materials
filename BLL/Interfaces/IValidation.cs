﻿namespace BLL.Interfaces
{
    public interface IValidation
    {
        bool IsStringValid(string text);
    }
}
