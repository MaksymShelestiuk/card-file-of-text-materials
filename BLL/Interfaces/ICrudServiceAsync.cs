﻿using BLL.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICrudServiceAsync<TDto> where TDto : class
    {
        Task<TDto> CreateAsync(TDto item);
        Task<bool> DeleteAsync(int id);
        Task<TDto> GetByIdAsync(int id);
        abstract Task<bool> UpdateAsync(TDto item);
        IEnumerable<TDto> Find(Func<TDto, Boolean> predicate);
    }
}
