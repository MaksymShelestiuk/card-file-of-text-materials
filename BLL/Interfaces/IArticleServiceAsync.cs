﻿using BLL.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IArticleServiceAsync : IValidation
    {
        Task<ArticleDto> GetByIdAsync(int id);
        Task<PagedList<ArticleDto>> GetAllAsync(ArticleParameters articleParameters);
        Task<PagedList<ArticleDto>> GetAllArticlesByUserIdAsync(int userProfileId, ArticleParameters articleParameters);
        Task<PagedList<ArticleDto>> GetAllArticlesByCategoryIdAsync(int categoryId, ArticleParameters articleParameters);
        Task<bool> UpdateAsync(ArticleDto item, int categoryId);
        Task<ArticleDto> CreateAsync(ArticleDto item, int categoryId);
        Task<bool> DeleteAsync(int id);
    }
}
