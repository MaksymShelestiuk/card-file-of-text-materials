﻿using AutoMapper;
using BLL.Dto;
using DAL.Entities;

namespace BLL.Mapping
{
    public class DtoMappingProfile : Profile
    {
        public DtoMappingProfile()
        {
            CreateMap<Article, ArticleDto>();
            CreateMap<Category, CategoryDto>();

            CreateMap<ArticleDto, Article>()
                .ForMember(entity => entity.CategoryId, opt => opt.MapFrom(dto => dto.Category.CategoryId))
                .ForMember(entity => entity.UserProfileId, opt => opt.MapFrom(dto => dto.UserProfile.Id));
            CreateMap<CategoryDto, Category>();
        }
    }
}
