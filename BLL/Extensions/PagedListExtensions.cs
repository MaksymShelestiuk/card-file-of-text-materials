﻿using BLL.Dto;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Extensions
{
    /// <summary>
    /// Provides extension methods for PagedList class
    /// </summary>
    public static class PagedListExtensions
    {
        /// <summary>
        /// Provides pagination data
        /// </summary>
        /// <param name="list">Collection of paginated UserProfile items</param>
        /// <returns></returns>
        public static object GetMetaData(this PagedList<UserProfile> list)
        {
            return new
            {
                list.TotalCount,
                list.PageSize,
                list.CurrentPage,
                list.TotalPages,
                list.HasNext,
                list.HasPrevious
            };
        }
        /// <summary>
        /// Provides pagination data
        /// </summary>
        /// <param name="list">Collection of paginated ArticleDto items</param>
        /// <returns></returns>
        public static object GetMetaData(this PagedList<ArticleDto> list)
        {
            return new
            {
                list.TotalCount,
                list.PageSize,
                list.CurrentPage,
                list.TotalPages,
                list.HasNext,
                list.HasPrevious
            };
        }
    }
}
