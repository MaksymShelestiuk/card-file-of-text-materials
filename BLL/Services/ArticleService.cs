﻿using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Mapping;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Dto;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Reflection;

namespace BLL.Services
{
    /// <summary>
    /// Provides functionality for Article entity to implement CRUD operations and also
    /// for pagination, searching, filtration and sorting ordered by some parameters.
    /// </summary>
    public class ArticleService : IArticleServiceAsync
    {
        private readonly IOrderByHelper<Article> _orderByHelper;
        IUnitOfWork Db { get; set; }
        /// <summary>
        /// Initializes Service for Article entity with UnitOfWork and OrderByHelper for sorting
        /// </summary>
        /// <param name="uow">Unit of work dependency</param>
        /// <param name="orderByHelper">Dependency for providing sort functionality</param>
        public ArticleService(IUnitOfWork uow, IOrderByHelper<Article> orderByHelper)
        {
            Db = uow;
            _orderByHelper = orderByHelper;
        }
        public async Task<ArticleDto> CreateAsync(ArticleDto item, int categoryId)
        {
            if (!IsStringValid(item.Title))
            {
                throw new ValidationException("Wrong Title of Article (First character must be letter and upper)", "");
            }
            item.Category = await Db.Categories.GetByIdAsync(categoryId);
            var result = await Db.Articles.CreateAsync(DtoMapper.Mapper.Map<ArticleDto, Article>(item));
            await Db.CommitAsync();
            return DtoMapper.Mapper.Map<Article, ArticleDto>(result);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            bool result = false;
            if (id != 0 && (await GetByIdAsync(id) != null))
            {
                result = await Db.Articles.DeleteAsync(id);
                await Db.CommitAsync();
                return result;
            }
            else
                return result;
        }

        public async Task<PagedList<ArticleDto>> GetAllAsync(ArticleParameters articleParameters)
        {
            var articles = Db.Articles.Find(a => a.DateOfPublication >= articleParameters.MinDate &&
            a.DateOfPublication <= articleParameters.MaxDate);
            SearchByTitle(ref articles, articleParameters.SearchTitle);
            var orderedArticles = _orderByHelper.ApplySort(articles, articleParameters.OrderBy);
            var source = DtoMapper.Mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDto>>(orderedArticles);
            var sourcePaged = PagedList<ArticleDto>.ToPagedList(source, articleParameters.PageNumber, articleParameters.PageSize);
            return sourcePaged;
        }
        public async Task<ArticleDto> GetByIdAsync(int id)
        {
            var result = await Db.Articles.GetWithDetailsByIdAsync(id);
            return DtoMapper.Mapper.Map<Article, ArticleDto>(result);
        }
        public async Task<PagedList<ArticleDto>> GetAllArticlesByUserIdAsync(int userProfileId, ArticleParameters articleParameters)
        {
            var articles = await Db.Articles.GetAllWithDetailsByUserIdAsync(userProfileId);
            articles = articles.Where(a => a.DateOfPublication >= articleParameters.MinDate &&
            a.DateOfPublication <= articleParameters.MaxDate);
            SearchByTitle(ref articles, articleParameters.SearchTitle);
            var source = DtoMapper.Mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDto>>(articles);
            await Db.CommitAsync();
            return PagedList<ArticleDto>.ToPagedList(source, articleParameters.PageNumber, articleParameters.PageSize);
        }
        public async Task<PagedList<ArticleDto>> GetAllArticlesByCategoryIdAsync(int categoryId, ArticleParameters articleParameters)
        {
            var articles = await Db.Articles.GetAllWithDetailsByCategoryIdAsync(categoryId);
            articles = articles.Where(a => a.DateOfPublication >= articleParameters.MinDate &&
            a.DateOfPublication <= articleParameters.MaxDate);
            SearchByTitle(ref articles, articleParameters.SearchTitle);
            var source = DtoMapper.Mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDto>>(articles);
            await Db.CommitAsync();
            return PagedList<ArticleDto>.ToPagedList(source, articleParameters.PageNumber, articleParameters.PageSize);
        }

        public async Task<bool> UpdateAsync(ArticleDto item, int categoryId)
        {
            var oldArticle = await Db.Articles.GetByIdAsync(item.ArticleId);
            if (oldArticle.CategoryId != categoryId)
            {
                var newCategory = await Db.Categories.GetByIdAsync(categoryId);
                oldArticle.Category = newCategory;
            }
            oldArticle.Title = item.Title;
            oldArticle.Description = item.Description;
            oldArticle.Content = item.Content;
            oldArticle.DateOfPublication = item.DateOfPublication;
            bool result = await Db.Articles.UpdateAsync(oldArticle);
            await Db.CommitAsync();
            return result;
        }
        private void SearchByTitle(ref IEnumerable<Article> articles, string title)
        {
            if (!articles.Any() || string.IsNullOrWhiteSpace(title))
                return;
            articles = articles.Where(a => a.Title.ToLower().Contains(title.Trim().ToLower()));
        }

        public bool IsStringValid(string text)
        {
            return text.Length > 0 && char.IsLetter(text[0]) && char.IsUpper(text[0]);
        }
    }
}