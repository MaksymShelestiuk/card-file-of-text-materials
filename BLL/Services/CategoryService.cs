﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Entities;
using BLL.Dto;
using BLL.Mapping;
using System.Linq;

namespace BLL.Services
{
    /// <summary>
    /// Provides functionality for Category entity to implement CRUD operations and also
    /// for searching by some parameters.
    /// </summary>
    public class CategoryService : ICategoryServiceAsync
    {
        IUnitOfWork Db { get; set; }
        /// <summary>
        /// Initializes Service for Category entity with UnitOfWork
        /// </summary>
        /// <param name="uow">Unit of work dependency</param>
        public CategoryService(IUnitOfWork uow)
        {
            Db = uow;
        }
        public async Task<CategoryDto> CreateAsync(CategoryDto item)
        {
            Category result;
            if (!IsStringValid(item.Name))
            {
                throw new ValidationException("Wrong Name of Category (First character of name must be letter and upper)", "");
            }
            else
            {
                result = await Db.Categories.CreateAsync(DtoMapper.Mapper.Map<CategoryDto, Category>(item));
                await Db.CommitAsync();
            }
            return DtoMapper.Mapper.Map<Category, CategoryDto>(result);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            bool result = false;
            if (id != 0 && (await GetByIdAsync(id) != null))
            {
                result = await Db.Categories.DeleteAsync(id);
                await Db.CommitAsync();
                return result;
            }
            else
                return result;
        }

        public async Task<IEnumerable<CategoryDto>> GetAllAsync(CategoryParameters categoryParameters)
        {
            var categories = await Db.Categories.GetAllAsync();
            SearchByName(ref categories, categoryParameters.SearchName);
            var result = DtoMapper.Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(categories);
            return result;
        }

        public async Task<CategoryDto> GetByIdAsync(int id)
        {
            var result = await Db.Categories.GetByIdAsync(id);
            return DtoMapper.Mapper.Map<Category, CategoryDto>(result);
        }

        public async Task<bool> UpdateAsync(CategoryDto item)
        {
            var oldcategory = await Db.Categories.GetByIdAsync(item.CategoryId);
            oldcategory.Name = item.Name;
            bool result = await Db.Categories.UpdateAsync(oldcategory);
            await Db.CommitAsync();
            return result;
        }

        public IEnumerable<CategoryDto> Find(Func<CategoryDto, bool> predicate)
        {
            var mapped = DtoMapper.Mapper.Map<Func<CategoryDto, bool>, Func<Category, bool>>(predicate);
            var result = Db.Categories.Find(mapped);
            return DtoMapper.Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(result);
        }
        private void SearchByName(ref IEnumerable<Category> categories, string name)
        {
            if (!categories.Any() || string.IsNullOrWhiteSpace(name))
                return;
            categories = categories.Where(c => c.Name.ToLower().Contains(name.Trim().ToLower()));
        }

        public bool IsStringValid(string text)
        {
            return text.Length > 0 && char.IsLetter(text[0]) && char.IsUpper(text[0]);
        }
    }
}

