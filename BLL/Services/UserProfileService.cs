﻿using BLL.Interfaces;
using DAL.Entities;
using BLL.Dto;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Provides functionality for UserProfile entity to implement some CRUD operations and also
    /// for pagination, searching and sorting ordered by some parameters.
    /// </summary>
    public class UserProfileService : IUserProfileService
    {
        private readonly UserManager<UserProfile> _userManager;
        private readonly IOrderByHelper<UserProfile> _orderByHelper;
        /// <summary>
        /// Initializes Service for UserProfile entity with UserManager and OrderByHelper for sorting
        /// </summary>
        /// <param name="userManager">Provides functionality for managing user</param>
        /// <param name="orderByHelper">Dependency for providing sort functionality</param>
        public UserProfileService(UserManager<UserProfile> userManager, IOrderByHelper<UserProfile> orderByHelper)
        {
            _userManager = userManager;
            _orderByHelper = orderByHelper;
        }

        public PagedList<UserProfile> GetAll(UserProfileParameters parameters)
        {
            var users = _userManager.Users;
            SearchByUserName(ref users, parameters.SearchUserName);
            var orderedUsers = _orderByHelper.ApplySort(users.ToList(), parameters.OrderBy);
            return PagedList<UserProfile>.ToPagedList(orderedUsers, parameters.PageNumber, parameters.PageSize);
        }
        public async Task<IdentityResult> UpdateAsync(int userId, SaveUserProfileDto saveUserProfile)
        {
            var userToUpdate = await _userManager.FindByIdAsync(userId.ToString());
            userToUpdate.Email = saveUserProfile.Email;
            userToUpdate.UserName = saveUserProfile.Email.Remove(saveUserProfile.Email.IndexOf("@"));
            userToUpdate.FirstName = saveUserProfile.FirstName;
            userToUpdate.SecondName = saveUserProfile.SecondName;
            userToUpdate.PhoneNumber = saveUserProfile.PhoneNumber;
            var userUpdateResult = await _userManager.UpdateAsync(userToUpdate);
            return userUpdateResult;
        }
        public async Task<UserProfile> GetByIdAsync(int id)
        {
            var result = await _userManager.FindByIdAsync(id.ToString());
            return result;
        }
        private void SearchByUserName(ref IQueryable<UserProfile> users, string username)
        {
            if (!users.Any() || string.IsNullOrWhiteSpace(username))
                return;
            users = users.Where(u => u.UserName.ToLower().Contains(username.Trim().ToLower()) || 
                u.SecondName.ToLower().Contains(username.Trim().ToLower()));
        }
    }
}