﻿using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Reflection;

namespace BLL
{
	/// <summary>
	/// Provides functionality for sorting items using orderByQuery string.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class OrderByHelper<T>: IOrderByHelper<T>
    {
		/// <summary>
		/// Sorts items using orderByQuery string that can contain properties of items with desc/asc or empty order of sorting. 
		/// Method dinamically generate OrderBy query, because orderByQuery string is previously unknown.
		/// </summary>
		/// <param name="items">Collection of items to sort</param>
		/// <param name="orderByQuery">String that contains query with orderBy parameters</param>
		/// <returns>Sorted collection of items ordered by the parameters from orderByQuery</returns>
		public IEnumerable<T> ApplySort(IEnumerable<T> items, string orderByQuery)
		{
			if (!items.Any())
				return items;
			if (string.IsNullOrWhiteSpace(orderByQuery))
			{
				return items;
			}
			// Split order query to have separate order statements (name,date desc) = {[name], [date desc]}
			var orderStatements = orderByQuery.Trim().Split(',');
			// Array of properties of T (to check if order query has real properties)
			var propertyInfos = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
			var orderQueryBuilder = new StringBuilder();

			foreach (var statement in orderStatements)
			{
				if (string.IsNullOrWhiteSpace(statement))
					continue;
				// Select property name string and then PropertyInfo from Array of properties
				var nameOfProperty = statement.Split(" ")[0];
				var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name.Equals(nameOfProperty, StringComparison.InvariantCultureIgnoreCase));
				if (objectProperty == null)
					continue;
				// If property really exists, check orderBy value
				var orderBy = statement.EndsWith(" desc") ? "descending" : "ascending";
				// Adding Property and orderBy parameters to sting for every iteration
				orderQueryBuilder.Append($"{objectProperty.Name.ToString()} {orderBy}, ");
			}
			// Final OrderBy string
			var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');
			return items.AsQueryable().OrderBy(orderQuery);
		}
	}
}
